
> - Collaboration: [Ableton](https://www.ableton.com/en/)
> - Distributed by [Ableton](https://www.ableton.com/en/packs/ircamax-2/)
> - [Technical Support](https://www.ableton.com/en/help/)

## Audio Effects ##

- **IM-PsychHarmonizer** is a harmonizer designed for vocal monophonic sounds. It separates signals’s estimated pitched and un-pitched components, which are then used to transpose the input sound (pitch part) and modulate the “prosody” (noise part). Two modes are available: Trans (transposition) and Voc (forced transposition). Additionally, either vocoder’s notes or harmonizer’s transpositions can be controlled via MIDI with the IM-MidiSend device allowing you to write harmonies using MIDI clips.

- **IM-Freezer** is intended to create sound textures, which are not necessarily meant to fit Live’s tempo. Rather you will freeze, scroll, and stretch a live or pre-recorded sound. The devices uses to separate synthesis engines: “Main”, which is a real time version of the [SuperVP](https://forum.ircam.fr/projects/detail/supervp-for-max/) program and “Contour”, which is a more classical granular synthesis. The use of a Contour sound is often needed when freezing a sound, and when the Main frozen signal sounds a bit unnatural. It is possible to balance the two signals in a Mix pane.

- **IM-Ianalyzer** is a real time polyphonic Audio to MIDI converter. It extracts signal’s peaks with various parameters and allows you to order them by frequency, sound pressure level, or psycho-acoustically perceived level or contribution to pitch perception. The resulting data can be resynthesized or send on an instrument track using the IM-MidiReceive device.

- **IM-Notetracker** is a real time Audio to MIDI converter. It extracts signal’s estimated pitch and amplitude, and converts them to MIDI notes components. An embedded synthesizer allows you to witness the analysis through sound. MIDI notes can be send to an IM-NotesReceive device, to be played back by an external instrument on a MIDI track.

- **IM-Chromax** is a spectral delay controlled by chords. The chords are converted into harmonics models that are used to define delay, feedback and filter shapes over frequencies. This results into a much more controllable spectral effect than when setting values by hand. According to how much of a given frequency the input sound contains, you will hear the pitches of the desired chord appearing in the processed sound. For instance, some special settings might turn the initial effect into a resonator filter effect.

- **IM-ModalysFilter** uses the Ircam [Modalys™](https://forum.ircam.fr/projects/detail/modalys/) physical modeling engine to filter the signal with two virtual resonators linked with a physical interaction. For example, use a circular membrane connected to a string by a strike connection to filter your track with a virtual snare drum or use two circular plates glued together to create a virtual gong…
	
## Instruments ##

- **IM-PlateSynth** is a polyphonic synthesizer based on Ircam [Modalys](https://forum.ircam.fr/projects/detail/modalys/)™ physical modeling engine. It allows to play on virtual plates or membranes to produce variety of sounds by changing the brightness, the damping or the harmonicity of the instrument. Two LFO’s can be used to modulate synthesis parameters, as the microphones positions to create natural phase effects.

- **IM-MatrixSynth** is a 3 oscillators, 3envelopes polyphonic synthesizer with a multimode filter. Oscillators can be “doubled” and detuned to add more liveness to the sound. The originality of this synth is its Modulation Matrix which allows to defined reinjections between oscillators (also on themselves!) to perform Frequency Modulation, Pulse Width modulation and oscillator resynchronization. It’s possible to define 4 different Matrix configurations and use a sequencer to decide which has to be played.

## IRCAMAX collection 2 ”Lesson“ ##

- **IM-Freezer**

IM-Freezer is intended to create sound textures which are not necessarily meant to fit Live’s tempo. Rather you will freeze, scroll, stretch a live or pre-recorded sound. The devices uses to separate synthesis engines: “Main”, which is a real time version of the [SuperVP](https://forum.ircam.fr/projects/detail/supervp-for-max/) program (used in IRCAM’s [Audiosculpt](https://forum.ircam.fr/projects/detail/audiosculpt/) software), and “Contour”, which is a more classical granular synthesis. The use of a Contour sound is often needed when freezing a sound, and when the Main frozen signal sounds a bit unnatural. It is possible to balance the two signals in the Mix pane.

*Pasted Graphic*

Using the fade time for Freeze, it is possible to set a signal deceleration (when freezing) or acceleration (when unfreezing).

Main controls the supervp signal. Features are:  time-stretching, transposition and formants transposition, spectral compression. Also adjust the FFT window size: longer sounds = greater window size. When transposing, supervp performs best when the maximal fundamental frequency is properly defined.

*Pasted Graphic 1*

Remix allows the separation and the mix of the harmonic, noise and transient sound:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/remix.png)

Contour lest you control the contour granular sound:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/contour-freeze.png)

Two details:

– When Contour is in Freeze mode (“Freeze” switch), the granular sound only performs when Freeze is enabled in the main UI. The Freeze fade time corresponds to the fade time for the Contour signals. Notice the small blue led next to the main Freeze switch that indicates that Contour is in freeze mode.

– Depending on the FFT window size for Main, and the GrainSize for Contour, some significant delays may appear. The switch next to Contour’s GrainSize allows to synchronize (as much as possible) both Main and Contour signals. This may lead into some delay in the parameter’s response, in case of very large values for GrainSize.

Note that there is no delay compensation setting here, since in most cases, input and processed signals will always be uncorrelated.

- **IM-Ianalyzer**

IM-Ianalyzer extracts signals’s peaks with various parameters and allows you to order them by frequency, sound pressure level, or psycho-acoustically perceived level or contribution to pitch perception

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/remix2.png)

The Tracking pane allows you to define when and how often one analysis frame is output by the analyzer. You can monitor the analysis thanks to an additive re-synthesis. Clicking on the different tabs (FFT, Peaks ,Output) lets you display some advanced settings for the analysis, peak extraction and output stages:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/peaks-extraction.png) ![](https://forum.ircam.fr/media/uploads/software/IRCAMAX%20Collection%202/spectrum-computation.png)![](https://forum.ircam.fr/media/uploads/software/IRCAMAX%20Collection%202/ouput-parameters.png)

Additionally, clicking on the blue arrow on the right lets you define some settings for an audio-to-midi-like conversion of the partials that are extracted:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/midi-partials_selection.png)

Select partials, filter the analysis using some thresholds, get rid of unwanted pitches, transform notes and send them to a MIDI instrument using an intermediate device – the IM-MidiReceive device:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-midireceive.png)

The analyzer was originally designed for voice’s peaks extraction, but it is rather versatile, and works well with many sound sources, including polyphonic ones. Of course the MIDI conversion is a quite rough process, and might not work as expected with any type of sound source, but you’ll get a good estimation of the sound’s timbre under the form of notes.

- **IM-NoteTracker**

IM-Notetracker extracts signals’s estimated pitch and amplitude, and converts them to MIDI notes’s components. An embedded synthesizer allows you to witness the analysis through sound. MIDI notes can be sent to an IM-NotesReceive device, to be played back by an external instrument in a MIDI track.

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-notetracker.png)

You can use some presets (voice, guitar…) in order to get started with parameters.

Additionally, the pitch estimation data can be used to control a remote parameter via the Live API. Click on the blue arrow in the Send Pane:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/send-pane.png)

This opens a Mapping area, similar to existing Mappers that can be found in the [M4L](https://www.ableton.com/en/live/max-for-live/) essentials or Tools. You have up to four simultaneous mappings. Notice that you can use either the raw pitch or note’s one (post-filters) as a controller.

- **IM-PsychHarmonizer**

IM-PsychHarmonizer is intended to be used for vocal monophonic sounds. It separates signals’s estimated pitched and un-pitched components, which are then used to transpose the input sound (pitch part) and modulate the “prosody” (noise part). Two modes are available: Trans (transposition) and Voc (forced transposition).

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-psychharmonizer.png)

Using internal LFOs it is possible to modulate both pitch and prosodic parts, in order to obtain some choral effects:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/voice.png)

Additionally, either vocoder’s notes or harmonizer’s transpositions can be controlled via MIDI, using the IM-MidiSend device:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-midisend.png)

MIDI transpositions are centered around the C3 pitch (C3 = no transposition, C2 = -12st, C4 = +12st).

- **IM-Chromax**

IM-Chromax uses some Gen code, derived from the generic Max example, which allows to set a different delay time and feedback amount per frequency bin. The main difference resides in the way you control delay and feedback times/amounts. Rather than setting delay times by hand (for each bin), it allows to define a chord. The chord is converted into an harmonic model, whose size and shape can be modified. This results into a much more controllable spectral effect than when setting the values by hand. According to how much of a given frequency the input sound contains, you will hear the pitches of the desired chord appearing in the processed sound. For instance, turning off the delays, and setting a great amount of feedback might turn the initial effect into a resonator-like one.

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-spectraldelay.png)

- **IM-ModalysFilter**

IM-ModalysFilter uses the [Modalys](https://forum.ircam.fr/projects/detail/modalys/)™ engine to filter the signal with two Resonators connected by an Interaction. The stereo signal is send to both resonators by default. You can change this balance using the InBal dial on the left. The OutBal dial sets the balance between the microphones of the two resonators.

By default, the device displays the configuration of Resonator 1. The Excitation section defines where the signal from Live is send to the resonator. The Microphone section defines where we hear our two objects. The center section allows to choose a new resonator or change its characteristics.

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-modalysfilter.png)

A second page show you the settings of Resonator 2:

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-modalysfilter-resonator2.png)

A third page displays the settings of the interaction: you can choose there the type of interaction which link the two resonators and its intensity.

On that page, you define also where the resonators are connected together.

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-modalysfilter-interaction.png)

The pitch of the two resonators can be controlled by MIDI using a IM-MidiSend device.

- **IM-PlateSynth (we need a new name for this one !)**

This device is a polyphonique Modalys synthesizer. It uses Modalys’s Plates or Membranes to generate sound.

The Excitator section allows to set if the object is strikes or excited by a filtered noise modulated by an envelope or both.

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-platesynth.png)

The Resonator section allows to choose an object type and define its physical characteristics. The object can be damped by [Modalys](https://forum.ircam.fr/projects/detail/modalys/) (Damper) or by an extra Decay envelope.

The center part allows to define the position of the Mallet, the Damper and two outputs on the objects.

The rightmost section sets the different modulations. Two LFOs (multiplied by an envelope) can be send each to two destinations.

- **IM-MatrixSynth**

This device is a 3 oscillators polyphonic synthesizer. Oscillators can “doubled” and detuned to add more liveness to the sound. Each of them is modulated by its own envelope (Env1, Env2, Env3). The 3 signals are added and send to the filter.

The power of this synth is its Modulation Matrix which allows to defined reinjections between the oscillators to perform Frequency Modulation, PulseWidth modulation and oscillator resynchronization. The matrix also has an ENV input (Modulation envelope) and an LFO input. The WHL input generates a signal proportional to the Modulation wheel of a MIDI keyboard (CC 1).

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAMAX%20Collection%202/im-matrixsynth.png)

It’s possible to define 4 different configurations. The device displays one matrix at a time. It’s possible to define a sequence of matrix to switch between the 4 configurations.


> - Collaboration: [Ableton](https://www.ableton.com/en/)
> - Only available by downloading
> - [Technical Support](https://www.ableton.com/en/help/)
> - **Contents:** 12 Max Devices, 75 Presets, 4 Live Sets
> - **Size:** Installation size: 176.47 MB; Download size: 160.1 MB
> - **Requirements:** Live 9 Standard (version 9.5 or higher); Max for Live 

> - [Vidéos](https://www.youtube.com/playlist?list=PL6MqWe5aRuOBxvpsMJjKC-tKF5jDm7k_f)









